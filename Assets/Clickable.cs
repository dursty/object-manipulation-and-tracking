﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clickable : InstructionReceiverInterface {

	public InstructionDatabase database;

	void OnMouseDown() {
        database.nextInstruction();
    }

	public override void onNewInstruction(InstructionStep instruction){

		if(instruction.getDisplayObjects().Count > 0){
	        DisplayObject obj = instruction.getDisplayObjects()[0];
	        if(obj != null){
	            InstructionDatabase s_Instance =  FindObjectOfType(typeof (InstructionDatabase)) as InstructionDatabase;
	            Instantiate(s_Instance.getPrefab(obj.getModel()), transform.position + obj.getTranslateCoord() , Quaternion.identity).Rotate(obj.getRotateCoord(),45);
	        }
		}
    }
}
