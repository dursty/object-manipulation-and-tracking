﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Windows.Speech;

public class FollowHand : InstructionReceiverInterface 
{



    private GameObject indicator = null;
    private TextMesh textMesh = null;
    private bool flagToStopObject = false;
    public GameObject Model;
    public GameObject Arrow1;
    public GameObject Arrow2;
    public GameObject Arrow3;
    public GameObject Arrow4;
    public GameObject Arrow5;
    private bool flagToTransform = false;
    public Vector3 currentPosition = new Vector3(0, 0, 0);

    // SpeechManager code START

    KeywordRecognizer keywordRecognizer = null;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();


    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }


    // SpeechManager code END

    private void CreateIndicator()
    {
        if (indicator == null)
        {
            indicator = Model;
            indicator.transform.localScale = new Vector3(0.04f, 0.04f, 0.04f);
        }
    }

    private void UpdateIndicator(Vector3 position)
    {
        if (indicator != null)
        {
            indicator.transform.position = position;
            var gazeDirection = Camera.main.transform.forward;
            gazeDirection.y = 0.0f;
            indicator.transform.rotation = Quaternion.LookRotation(gazeDirection);
            currentPosition = new Vector3(position.x, position.y + 0.1f, position.z);
        }
    }

    private void CreateText()
    {
        GameObject text = new GameObject();
        textMesh = text.AddComponent<TextMesh>();
        //text.AddComponent<GameObject>();
        text.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
    }

    private void UpdateText(Vector3 position, Vector3 velocity)
    {
        if (textMesh != null)
        {
            position = new Vector3(position.x, position.y + 0.1f, position.z);
            textMesh.gameObject.transform.position = position;
            var gazeDirection = Camera.main.transform.forward;
            textMesh.gameObject.transform.rotation = Quaternion.LookRotation(gazeDirection);
            textMesh.text = string.Format("Position:{0:0.00},{1:0.00},{2:0.00}\n Velocity: {3:0.00},{4:0.00},{5:0.00}", position.x, position.y, position.z, velocity.x, velocity.y, velocity.z);
        }
    }

    public void ShowObjects(bool show)
    {
        if (indicator != null && textMesh != null)
        {
            indicator.SetActive(show);
            textMesh.gameObject.SetActive(show);
        }
    }

    void Start()
    {

        // CODE FOR SPEECH RECOGNITION START


        // code to hide arrows upon application start
        //Arrow1.GetComponent<Renderer>().enabled = false;
        Arrow2.GetComponent<Renderer>().enabled = false;
        Arrow3.GetComponent<Renderer>().enabled = false;
        Arrow4.GetComponent<Renderer>().enabled = false;
        Arrow5.GetComponent<Renderer>().enabled = false;

        // code for keywords
        keywords.Add("Stop Model", () =>
        {
            flagToStopObject = true;
        });

        //keywords.Add("Show Arrow", showArrow);
        //keywords.Add("Show Arrow 2", showArrow2);
        //keywords.Add("Show Arrow 3", showArrow3);
        //keywords.Add("Show Arrow 4", showArrow4);
        //keywords.Add("Show Arrow 5", showArrow5);

        keywords.Add("Hide Arrow", hideArrow1);

        keywords.Add("Show Arrow", showArrow1);

        keywords.Add("Move Arrow", moveArrow1To);

        keywords.Add("Rotate Arrow", rotateArrow1);



        keywords.Add("Start Model", () =>
        {
            flagToStopObject = false;

        });

        keywords.Add("Hide Model", () =>
        {
            flagToStopObject = true;
            indicator.GetComponent<Renderer>().enabled = false;
            //UI.SetActive(false);

        });

        keywords.Add("Show Model", () =>
        {
            flagToStopObject = true;
            indicator.GetComponent<Renderer>().enabled = true;
            //UI.SetActive(true);

        });

        keywords.Add("Translate Model", () =>
        {
            //flagToTransform = true;
            indicator.transform.Translate(0, 0, 0.5f);
            //UI.SetActive(true);


        });

       


        // Tell the KeywordRecognizer about our keywords.
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());

        // Register a callback for the KeywordRecognizer and start recognizing!
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
        // CODE FOR SPEECH RECOGNITION END

        CreateIndicator();
        CreateText();
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceLostLegacy += InteractionManager_SourceLost;
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceDetectedLegacy += InteractionManager_SourceDetected;
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceUpdatedLegacy += InteractionManager_SourceUpdated;


    }

    private void InteractionManager_SourceDetected(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand)
        {
            ShowObjects(true);
        }
    }

    private void InteractionManager_SourceLost(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand && flagToStopObject == false)
        {
            ShowObjects(false);
        }
    }

    private void InteractionManager_SourceUpdated(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand)
        {
            Vector3 handPosition;
            Vector3 handVelocity;

            state.sourcePose.TryGetPosition(out handPosition);
            state.sourcePose.TryGetVelocity(out handVelocity);


            /*
            if (flagToTransform == true)
            {
                transform.Translate(0, 0, Time.deltaTime);
            }

            */


            //handPosition.y = handPosition.y - 0.1f;
            //handPosition.y = handPosition.y - 0.1f;

            if (flagToStopObject == false)
            {
                UpdateText(handPosition, handVelocity);
                UpdateIndicator(handPosition);
            }
        }
    }

    public override void onNewInstruction(InstructionStep instruction) {
        List<DisplayObject> objects = instruction.getDisplayObjects();
        for(int i = 0; i < objects.Count; i++)
        {
            hideAllArrows();
            showArrow(i);
            moveArrow(i, objects[i].getTranslateCoord());


        }
    }

    private void moveArrow(int i,Vector3 coord)
    {
        switch (i)
        {
            case 0:
                Arrow1.transform.Translate(coord);
                break;
            case 1:
                Arrow2.transform.Translate(coord);
                break;
            case 2:
                Arrow3.transform.Translate(coord);
                break;
            case 3:
                Arrow4.transform.Translate(coord);
                break;
            case 4:
                Arrow5.transform.Translate(coord);
                break;
        }
    }

    // Vector up = Vector3(0, 1, 0)
    // Vector right = Vector3(1, 0, 0)
    // Vector forward = Vector3(0, 0, 1)
    // Vector back = Vector3(0, 0, -1)


    private void rotateArrow(int i, Vector3 direction, int angle)
    {
        switch (i)
        {
            case 0:
                Arrow1.transform.rotation = Quaternion.identity;
                Arrow1.transform.rotation = Quaternion.AngleAxis(angle, direction);
                break;
            case 1:
                Arrow2.transform.rotation = Quaternion.identity;
                Arrow2.transform.rotation = Quaternion.AngleAxis(angle, direction);
                break;
            case 2:
                Arrow3.transform.rotation = Quaternion.identity;
                Arrow3.transform.rotation = Quaternion.AngleAxis(angle, direction);
                break;
            case 3:
                Arrow4.transform.rotation = Quaternion.identity;
                Arrow4.transform.rotation = Quaternion.AngleAxis(angle, direction);
                break;
            case 4:
                Arrow5.transform.rotation = Quaternion.identity;
                Arrow5.transform.rotation = Quaternion.AngleAxis(angle, direction);
                break;
        }
    }




    private void hideAllArrows()
    {
    
        Arrow1.GetComponent<Renderer>().enabled = false;
        Arrow2.GetComponent<Renderer>().enabled = false;
        Arrow3.GetComponent<Renderer>().enabled = false;
        Arrow4.GetComponent<Renderer>().enabled = false;
        Arrow5.GetComponent<Renderer>().enabled = false;
    }


    private void showArrow(int i)
    {
        switch (i)
        {
            case 0:
                Arrow1.GetComponent<Renderer>().enabled = true;
                break;
            case 1:
                Arrow2.GetComponent<Renderer>().enabled = true;
                break;
            case 2:
                Arrow3.GetComponent<Renderer>().enabled = true;
                break;
            case 3:
                Arrow4.GetComponent<Renderer>().enabled = true;
                break;
            case 4:
                Arrow5.GetComponent<Renderer>().enabled = true;
                break;
        }
    }


    // code to move arrow 1
    private void moveArrow1To()

    {
        Arrow1.transform.Translate(-1.3f, 0, 0);
    }


    // code to rotate arrow 1
    private void rotateArrow1()

    {
        Arrow1.transform.rotation = Quaternion.AngleAxis(30, Vector3.right);
    }

    // code to show arrow 1
    private void showArrow1()

    {
        Arrow1.GetComponent<Renderer>().enabled = true;
        Debug.Log("Woking");
    }

    // code to hide arrow 1
    private void hideArrow1()

    {
        Arrow1.GetComponent<Renderer>().enabled = false;
    }


}