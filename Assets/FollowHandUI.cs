﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Windows.Speech;

public class FollowHandUI : MonoBehaviour
{


    public InstructionDatabase database;

    private GameObject indicator = null;
    private TextMesh textMesh = null;
    private bool flagToStopObject = false;
    public GameObject UI;
    public GameObject instructionText;


    // SpeechManager code START

    KeywordRecognizer keywordRecognizer = null;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();


    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction))
        {
            keywordAction.Invoke();
        }
    }


    // SpeechManager code END

    private void CreateIndicator()
    {
        if (indicator == null)
        {

            indicator = UI;
            indicator.transform.localScale = new Vector3(0.04f, 0.04f, 0.04f);
        }
    }

    private void UpdateIndicator(Vector3 position)
    {
        if (indicator != null)
        {

            indicator.transform.position = position;
        }
    }

    private void CreateText()
    {
        GameObject text = new GameObject();
        textMesh = text.AddComponent<TextMesh>();
        //text.AddComponent<GameObject>();
        text.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
    }

    private void UpdateText(Vector3 position, Vector3 velocity)
    {
        if (textMesh != null)
        {
            position = new Vector3(position.x, position.y + 0.1f, position.z);
            textMesh.gameObject.transform.position = position;
            var gazeDirection = Camera.main.transform.forward;
            textMesh.gameObject.transform.rotation = Quaternion.LookRotation(gazeDirection);
            textMesh.text = string.Format("Position:{0:0.00},{1:0.00},{2:0.00}\n Velocity: {3:0.00},{4:0.00},{5:0.00}", position.x, position.y, position.z, velocity.x, velocity.y, velocity.z);
        }
    }

    public void ShowObjects(bool show)
    {
        if (indicator != null && textMesh != null)
        {
            indicator.SetActive(show);
            textMesh.gameObject.SetActive(show);
        }
    }

    void Start()
    {

        // CODE FOR SPEECH RECOGNITION START
        keywords.Add("Next",() => {
                database.nextInstruction();
        });


        keywords.Add("Stop UI", () =>
        {
            flagToStopObject = true;
        });



        keywords.Add("Start UI", () =>
    {
        flagToStopObject = false;

    });

        keywords.Add("Hide UI", () =>
        {
            flagToStopObject = true;
            indicator.GetComponent<Renderer>().enabled = false;
            instructionText.GetComponent<Renderer>().enabled = false;

        //UI.SetActive(false);

    });

        keywords.Add("Show UI", () =>
        {
            flagToStopObject = true;
            indicator.GetComponent<Renderer>().enabled = true;
            instructionText.GetComponent<Renderer>().enabled = true;
        //UI.SetActive(true);

    });


        // Tell the KeywordRecognizer about our keywords.
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());

        // Register a callback for the KeywordRecognizer and start recognizing!
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
        // CODE FOR SPEECH RECOGNITION END

        CreateIndicator();
        CreateText();
        alwaysStayOriented();
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceLostLegacy += InteractionManager_SourceLost;
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceDetectedLegacy += InteractionManager_SourceDetected;
        UnityEngine.XR.WSA.Input.InteractionManager.InteractionSourceUpdatedLegacy += InteractionManager_SourceUpdated;
    }

    private void InteractionManager_SourceDetected(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        //alwaysStayOriented();
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand)
        {
            ShowObjects(true);

        }
    }

    private void InteractionManager_SourceLost(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        //alwaysStayOriented();
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand && flagToStopObject == false)
        {
            ShowObjects(false);

        }
    }

    private void InteractionManager_SourceUpdated(UnityEngine.XR.WSA.Input.InteractionSourceState state)
    {
        alwaysStayOriented();
        if (state.source.kind == UnityEngine.XR.WSA.Input.InteractionSourceKind.Hand)
        {
            Vector3 handPosition;
            Vector3 handVelocity;

            state.sourcePose.TryGetPosition(out handPosition);
            state.sourcePose.TryGetVelocity(out handVelocity);


            //handPosition.z = handPosition.z + 0.8f;
            if (flagToStopObject == false)
            {

                UpdateText(handPosition, handVelocity);
                UpdateIndicator(handPosition);
            }


        }
    }


    private void alwaysStayOriented()
    {
        var gazeDirection = Camera.main.transform.forward;
        gazeDirection.y = 0.0f;
        indicator.transform.rotation = Quaternion.LookRotation(gazeDirection);
        indicator.transform.Rotate(Vector3.right * (-90));
    }


}
