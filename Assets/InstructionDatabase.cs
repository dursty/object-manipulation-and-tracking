﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionDatabase : MonoBehaviour {

	public List<InstructionReceiverInterface> receivers;
	public Transform arrowPrefab;
	public InstructionSectionInterface sectionText;
	public TextAsset xmlFile;

	private InstructionSet instructionSet;
	static List<InstructionStep> currentInstructions;
	static int currentSection = 0;
	static int currentInstruction = 0;

	// Use this for initialization
	void Start () {
		instructionSet = InstructionSet.loadFromFile(xmlFile.text);

		currentInstructions = instructionSet.getInstructions(currentSection);
		sectionText.onNewSection(instructionSet.getSection(currentSection));
	}

	public InstructionStep nextInstruction(){
		// End of this section
		if(currentInstruction >= currentInstructions.Count){
			currentSection++;
			currentInstruction = 0;

			if(currentSection < instructionSet.getSectionCount()){
				currentInstructions = instructionSet.getInstructions(currentSection);
				sectionText.onNewSection(instructionSet.getSection(currentSection));
			}else{
				// return last instruction if overflow, should handle this better
				return currentInstructions[currentInstructions.Count -1];
			}
		}

		InstructionStep instruction = currentInstructions[currentInstruction];
		currentInstruction++;

		for(int i = 0;i<receivers.Count;i++){
			receivers[i].onNewInstruction(instruction);
		}

		return instruction;
	}

	public Transform getPrefab(DisplayObject.ModelType modelType){
		if(modelType == DisplayObject.ModelType.Arrow){
			return arrowPrefab;
		}else{
			return null;
		}
	}
}
