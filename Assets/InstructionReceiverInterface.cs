﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InstructionReceiverInterface : MonoBehaviour {

	public int ID;

	public abstract void onNewInstruction(InstructionStep instruction);
}
