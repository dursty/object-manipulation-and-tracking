﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.WSA.Input;

public class InstructionScript : MonoBehaviour
{

    // Holds the default size of the text
    public TextMesh instructionText;

    // Holds the camera object
    public GameObject camera;

    private GestureRecognizer recognizer;

    // Use this for initialization
    void Start()
    {
        setupGesture();

        initializeInstructionText();
    }

    // Update is called once per frame
    void Update()
    {
        // Rotates the object to face the camera object
        var camPos = transform.position - camera.transform.position;
        camPos.y = transform.position.y;
        this.transform.LookAt(camera.transform.position);
        this.transform.Rotate(Vector3.up, 180);
    }

    // Sets up tap controls (may not  be used)
    private void setupGesture()
    {
        recognizer = new GestureRecognizer();
        recognizer.SetRecognizableGestures(GestureSettings.Tap);
        recognizer.TappedEvent += MyTapEventHandler;
        recognizer.StartCapturingGestures();

    }

    // Not currently used
    private void MyTapEventHandler(InteractionSourceKind source, int tapCount, Ray headRay)
    {
        Debug.Log("Tap");
    }

    private void initializeInstructionText()
    {
        // Gets the render box for locating the text object
        var rend = gameObject.GetComponent<Renderer>();
        // Places text object in top left corner
        Vector3 textPos = new Vector3(rend.bounds.min.x, rend.bounds.max.y, rend.bounds.max.z);
        var textObject = Instantiate(instructionText);
        textObject.transform.parent = gameObject.transform;
        textObject.transform.position = textPos;
        textObject.text = "Testing the dsfsdafsdf asdfas dasd fasd asd";
    }
}
