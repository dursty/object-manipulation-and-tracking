﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InstructionSectionInterface : MonoBehaviour {

	public abstract void onNewSection(InstructionSection section);
}
