using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

 [XmlRoot("InstructionSet")]
public class InstructionSet {

    [XmlAttribute("name")]
    public string name;

    [XmlArray("Sections")]
 	[XmlArrayItem("Section")]
	public List<InstructionSection> sections;

    public InstructionSection getSection(int i){
        return sections[i];
    }

    public List<InstructionStep> getInstructions(int i){
        return sections[i].getInstructions();
    }

    public string getName(){
        return name;
    }

    public int getSectionCount(){
        return sections.Count;
    }

    public static InstructionSet loadFromFile(string filename){
        var serializer = new XmlSerializer(typeof(InstructionSet));
        using(var reader = new System.IO.StringReader(filename)){
             return serializer.Deserialize(reader) as InstructionSet;
        }
        /*
        var stream = new FileStream(filename,FileMode.Open);
        InstructionSet set = serializer.Deserialize(stream) as InstructionSet;
        return set;*/
    }
}
