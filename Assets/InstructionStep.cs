﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

public class InstructionStep {
	[XmlAttribute("id")]
	public int id;

	[XmlArray("Texts")]
    [XmlArrayItem("Item")]
	public List<string> text;

	[XmlArray("DisplayObjects")]
	[XmlArrayItem("DisplayObject")]
	public List<DisplayObject> DisplayObjects;

	public static InstructionStep loadFromFile(string filename){
        var serializer = new XmlSerializer(typeof(InstructionStep));
        var stream = new FileStream(filename,FileMode.Open);
        return serializer.Deserialize(stream) as InstructionStep;
    }

	public void setDisplayObjects(List<DisplayObject> objs){
		this.DisplayObjects = objs;
	}

	public int getID(){
		return id;
	}

	public List<string> getText(){
		return text;
	}

	public List<DisplayObject> getDisplayObjects(){
		return DisplayObjects;
	}
}
