﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SampleInstructionReceiver : InstructionReceiverInterface {

    private TextMesh txt;

    void Start(){
        txt = GetComponent<TextMesh>();
    }

	public override void onNewInstruction(InstructionStep instruction){
        InstructionText wrapper = new InstructionText();
        float cWidth = wrapper.getCharacterWidth(txt);
        float pWidth = wrapper.getPanelWidth(txt.transform.parent);
        int maxChar = (int)(pWidth/cWidth);

		txt.text = wrapper.fixOverflow(instruction.getText()[0],maxChar*2);
    }
}
